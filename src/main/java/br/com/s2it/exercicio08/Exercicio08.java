package br.com.s2it.exercicio08;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.IntStream;

public class Exercicio08 {

    public Integer a;
    public Integer b;
    public Integer c;

    public Exercicio08(Integer a, Integer b) {
        this.a = a;
        this.b = b;
        this.c = buildNumeroC(a, b);
    }

    private Integer buildNumeroC(Integer a, Integer b) {
        if(a == null || b == null) {
            return null;
        }
        int[] digitsA = Integer.toString(a).chars().map(c -> c -= '0').toArray();
        int[] digitsB = Integer.toString(b).chars().map(c -> c -= '0').toArray();
        String retorno = "";
        String digitoAuxiliarA = "";
        String digitoAuxiliarB = "";

        if (digitsA.length == digitsB.length) {
            for (int i = 0; i < digitsA.length; i++) {
                digitoAuxiliarB = String.valueOf(digitsB[i]);
                digitoAuxiliarA = String.valueOf(digitsA[i]);
                retorno += digitoAuxiliarA + digitoAuxiliarB;
            }
            return validaERetornaNumeroC(retorno);
        }
        if (digitsA.length > digitsB.length) {
            for (int i = 0; i < digitsA.length; i++) {
                if(i >= digitsB.length) {
                    digitoAuxiliarB = String.valueOf(digitsA[i]);
                } else {
                    digitoAuxiliarB = String.valueOf(digitsB[i]);
                }
                retorno += String.valueOf(digitsA[i]) + digitoAuxiliarB;
            }
            return validaERetornaNumeroC(retorno);
        }
        if (digitsA.length < digitsB.length) {
            for (int i = 0; i < digitsB.length; i++) {
                if(i >= digitsA.length) {
                    digitoAuxiliarB = String.valueOf(digitsB[i]);
                    digitoAuxiliarA = digitoAuxiliarB;
                } else {
                    digitoAuxiliarB = String.valueOf(digitsB[i]);
                    digitoAuxiliarA = String.valueOf(digitsA[i]);
                }
                retorno += digitoAuxiliarA + digitoAuxiliarB;
            }
            return validaERetornaNumeroC(retorno);
        }
        return null;
    }

    private Integer validaERetornaNumeroC(String retorno) {
        BigDecimal valorDoNumeroC = new BigDecimal(retorno);
        if (valorDoNumeroC.compareTo(new BigDecimal("1000000")) == 1) {
            return -1;
        }
        return valorDoNumeroC.intValue();
    }

    private int getLength(Integer number) {
        return (int) (Math.log10(number) + 1);
    }

    public Integer getA() {
        return a;
    }

    public Integer getB() {
        return b;
    }

    public Integer getC() {
        return c;
    }
}
