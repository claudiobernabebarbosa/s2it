package br.com.s2it.exercicio09;

public class BinaryTree {

    int valor;
    BinaryTree left;
    BinaryTree right;

    public BinaryTree(int valor) {
        this.valor = valor;
        this.left = null;
        this.right = null;
    }

}
