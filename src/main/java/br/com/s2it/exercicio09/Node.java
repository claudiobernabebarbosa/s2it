package br.com.s2it.exercicio09;

public class Node {

    BinaryTree arvoreBinaria;

    boolean eValido(BinaryTree node) {
        if(node == null) {
            return false;
        }
        if(node.left == null && node.right == null) {
            return true;
        }
        return false;
    }

    public int somaTudo(BinaryTree arvoreBinaria) {
        int retorno = 0;
        if(arvoreBinaria != null) {
            if(eValido(arvoreBinaria.left)) {
                retorno += arvoreBinaria.left.valor;
            }
            if(eValido(arvoreBinaria.right)) {
                retorno += arvoreBinaria.right.valor;
            }
            retorno += somaTudo(arvoreBinaria.right);
            retorno += somaTudo(arvoreBinaria.left);
        }
        return retorno;
    }

    public int fazerASomaDeTodosOsNosDaEsquerda(BinaryTree arvoreBinaria) {
        int retorno = 0;
        if(arvoreBinaria != null) {
            if(eValido(arvoreBinaria.left)) {
                retorno += arvoreBinaria.left.valor;
            } else {
                retorno += fazerASomaDeTodosOsNosDaEsquerda(arvoreBinaria.left);
            }
            retorno += fazerASomaDeTodosOsNosDaEsquerda(arvoreBinaria.right);
        }
        return retorno;
    }

    public int fazerASomaDeTodosOsNosDaDireita(BinaryTree node) {
        int retorno = 0;
        if(node != null) {
            if(eValido(node.right)) {
                retorno += node.right.valor;
            } else {
                retorno += fazerASomaDeTodosOsNosDaDireita(node.right);
            }
            retorno += fazerASomaDeTodosOsNosDaDireita(node.left);
        }
        return retorno;
    }

}
