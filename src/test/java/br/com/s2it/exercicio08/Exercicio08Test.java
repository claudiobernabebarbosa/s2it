package br.com.s2it.exercicio08;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class Exercicio08Test {

    @Test
    public void declarandoDoisInteirosQueCombinamMaiorQue1000000() {
        Exercicio08 exercicio08 = new Exercicio08(874585, 874585);
        Integer esperado = -1;
        Assert.assertEquals(esperado, exercicio08.c);
    }

    @Test
    public void declarandoDoisInteirosComOMesmoNumerodeDigitos() {
        Exercicio08 exercicio08 = new Exercicio08(412, 124);
        Integer esperado = 411224;
        Assert.assertEquals(esperado, exercicio08.c);
    }

    @Test
    public void declarandoDoisInteirosSendoBMenorQueA() {
        Exercicio08 exercicio08 = new Exercicio08(102, 51);
        Integer esperado = 150122;
        Assert.assertEquals(esperado, exercicio08.c);
    }

    @Test
    public void declarandoDoisInteirosSendoAMenorQueB() {
        Exercicio08 exercicio08 = new Exercicio08(51, 102);
        Integer esperado = 511022;
        Assert.assertEquals(esperado, exercicio08.c);
    }

    @Test
    public void declarandoDoisInteirosSendoANulo() {
        Exercicio08 exercicio08 = new Exercicio08(null, 102);
        Integer esperado = null;
        Assert.assertEquals(esperado, exercicio08.c);
    }

    @Test
    public void declarandoDoisInteirosSendoBNulo() {
        Exercicio08 exercicio08 = new Exercicio08(51, null);
        Integer esperado = null;
        Assert.assertEquals(esperado, exercicio08.c);
    }

    @Test
    public void declarandoDoisInteirosSendoANuloBNulo() {
        Exercicio08 exercicio08 = new Exercicio08(null, null);
        Integer esperado = null;
        Assert.assertEquals(esperado, exercicio08.c);
    }
}
