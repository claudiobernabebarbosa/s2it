package br.com.s2it.exercicio09;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class BinaryTreeTest {

    private Node tree;

    @Test
    public void somarTodosOsElementos() {
        preencheBinaryTree();
        Integer soma = tree.somaTudo(tree.arvoreBinaria);
        Integer esperado = 102;
        assertEquals(esperado, soma);
    }

    @Test
    public void quandoTentoSomarUmaArvoreNula() {
        tree = new Node();
        Integer soma = tree.somaTudo(tree.arvoreBinaria);
        Integer esperado = 0;
        assertEquals(esperado, soma);
    }

    @Test
    public void somarTodosOsElementosAPartirDeUmaArvoreBinaria()  {
        preencheBinaryTree();
        Integer somaElementosDaEsquerda = tree.somaTudo(tree.arvoreBinaria.left);
        Integer esperado = 17;
        assertEquals(esperado, somaElementosDaEsquerda);
    }

    private void preencheBinaryTree() {
        tree = new Node();
        tree.arvoreBinaria = new BinaryTree(20);
        tree.arvoreBinaria.left = new BinaryTree(9);
        tree.arvoreBinaria.right = new BinaryTree(49);
        tree.arvoreBinaria.left.right = new BinaryTree(12);
        tree.arvoreBinaria.left.left = new BinaryTree(5);
        tree.arvoreBinaria.right.left = new BinaryTree(23);
        tree.arvoreBinaria.right.right = new BinaryTree(52);
        tree.arvoreBinaria.right.right.right = new BinaryTree(12);
        tree.arvoreBinaria.right.right.left = new BinaryTree(50);
    }
}
